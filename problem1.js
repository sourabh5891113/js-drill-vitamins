function availableItems(items) {
  if (Array.isArray(items)) {
    const availableItems = items.filter((item) => item.available);
    return availableItems;
  } else {
    console.log("First argument should be an Array");
    return [];
  }
}

module.exports = availableItems;

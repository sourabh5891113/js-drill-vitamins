/*
4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
*/
let items = require("../3-arrays-vitamins.cjs");
let groupByVitamins = require("../problem4");

console.log(groupByVitamins(items));

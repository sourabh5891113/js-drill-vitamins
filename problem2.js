function containsOnlyVC(items) {
  if (Array.isArray(items)) {
    const containsOnlyVC = items.filter(
      (item) => item.contains === "Vitamin C"
    );
    return containsOnlyVC;
  } else {
    console.log("First argument should be an Array");
    return [];
  }
}

module.exports = containsOnlyVC;

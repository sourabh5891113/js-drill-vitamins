function containsVA(items) {
  if (Array.isArray(items)) {
    const containsVA = items.filter((item) =>
      item.contains.includes("Vitamin A")
    );
    return containsVA;
  } else {
    console.log("First argument should be an Array");
    return [];
  }
}

module.exports = containsVA;

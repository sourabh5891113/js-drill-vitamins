function groupByVitamins(items) {
  if (Array.isArray(items)) {
    let groupByVitamins = items.reduce((acc, item) => {
      let name = item.name;
      let containArr = item.contains.split(", ");
      for (let key of containArr) {
        if (acc[key]) {
          acc[key] = [...acc[key], name];
        } else {
          acc[key] = [name];
        }
      }
      return acc;
    }, {});
    return groupByVitamins;
  } else {
    console.log("First argument should be an Array");
    return [];
  }
}

module.exports = groupByVitamins;

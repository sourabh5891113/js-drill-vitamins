function sortByNOVitamin(items) {
  if (Array.isArray(items)) {
    items.sort((a, b) => {
      return a.contains.split(", ").length - b.contains.split(", ").length;
    });
    return items;
  } else {
    console.log("First argument should be an Array");
    return [];
  }
}

module.exports = sortByNOVitamin;
